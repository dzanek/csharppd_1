﻿using System;

namespace ConsoleApplication1.Zad3
{
    public static class Zad32
    {
        public static void IsNumberDivisorOfAnother()
        {
            Console.WriteLine("Podaj pierwszą liczbę całkowitą:");
            var first = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Podaj drugą liczbę całkowitą:");
            var second = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine(IsDivisor(first, second)
                ? $"Liczba: {second} jest dzielnikiem {first}"
                : $"Liczba: {second} nie jest dzielnikiem {first}");
        }

        static bool IsDivisor(int first, int second)
        {
            return first % second == 0;
        }
    }
}
