﻿using System;
using System.Collections.Generic;

namespace ConsoleApplication1.Zad3
{
    public static class Zad35
    {
        public static void FindEquationSolutions()
        {
            Console.WriteLine("Podaj współczynnik a równania:");
            var a = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Podaj współczynnik b równania:");
            var b = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Podaj współczynnik c równania:");
            var c = Convert.ToDouble(Console.ReadLine());
            var solutions = SolveEquation(a, b, c);
            if (solutions.Count > 0)
            {
                Console.Write($"Rozwiązania równania {a}x^2 + {b}x + {c} to : ");
                foreach (var x in solutions)
                {
                    Console.Write($"({x}) ");
                }
            }
            else
            {
                Console.Write($"Równanie {a}x^2 + {b}x + {c} nie ma rozwiązań w dziedzinie rzeczywistej.");
            }
        }

        private static List<double> SolveEquation(double a, double b, double c)
        {
            var solutions = new List<double>();
            var delta = CalculateDelta(a, b, c);
            if (delta == 0)
            {
                solutions.Add(-b / 2 * a);
            }
            else if (delta > 0)
            {
                solutions.Add((-b + Math.Sqrt(delta)) / (2 * a));
                solutions.Add((-b - Math.Sqrt(delta)) / (2 * a));
            }

            return solutions;
        }

        private static double CalculateDelta(double a, double b, double c)
        {
            return b * b - 4 * a * c;
        }
    }
}