﻿using System;

namespace ConsoleApplication1.Zad3
{
    public static class Zad313
    {
        public static void SumOfSeries()
        {
            Console.WriteLine("Obliczanie sumy szeregu 1 − 2 + 3 − 4 + … n, podaj n: ");
            var n = Convert.ToInt32(Console.ReadLine());
            var suma = 0;
            for (var i = 1; i <= n; i++)
            {
                suma += (int) (i * Math.Pow(-1, i - 1));
            }

            Console.WriteLine($"Suma szeregu wynosi {suma}");
        }
    }
}