﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleApplication1.Zad3
{
    public static class Zad315
    {
        public static void Coins()
        {
            var coins = new List<int>();
            var amounts = new List<int> {1, 2, 5};
            Console.WriteLine("Dysponując monetami 1 zł, 2 zł i 10 zł można wypłacić 10 zł w następujące sposoby:");
            GetCombinations(coins, amounts, 0, 0, 10);
        }

        private static void GetCombinations(List<int> coins, List<int> amounts, int highest, int sum, int goal)
        {
            if (sum == goal)
            {
                Print(coins, amounts);
                return;
            }

            if (sum > goal)
            {
                return;
            }

            foreach (var value in amounts)
            {
                if (value < highest) continue;
                var copy = new List<int>(coins) {value};
                GetCombinations(copy, amounts, value, sum + value, goal);
            }
        }

        private static void Print(List<int> coins, List<int> amounts)
        {
            Console.Write("[ ");
            foreach (var amount in amounts)
            {
                var count = coins.Count(value => value == amount);
                for (var i = 0; i < count; i++)
                {
                    Console.Write($"{amount} ");
                }
            }

            Console.WriteLine("]");
        }
    }
}