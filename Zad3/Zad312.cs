﻿using System;
using System.Collections.Generic;

namespace ConsoleApplication1.Zad3
{
    public static class Zad312
    {
        public static void ReadNumbersTillZero()
        {
            var ints = new List<int>();
            for (;;)
            {
                var n = Convert.ToInt32(Console.ReadLine());
                ints.Add(n);
                if (n == 0)
                {
                    break;
                }
            }
            Console.Write("Wprowadzone liczby: ");
            ints.ForEach(i => Console.Write("{0} ", i));
        }
    }
}
