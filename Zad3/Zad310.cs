﻿using System;
using System.Linq;

namespace ConsoleApplication1.Zad3
{
    public static class Zad310
    {
        public static void Factorial()
        {
            Console.WriteLine("Podaj liczbę n dla której chcesz obliczyć silnię:");
            var n = Convert.ToInt32(Console.ReadLine());
            int i, fact = 1;
            for (i = 1; i <= n; i++)
            {
                fact *= i;
            }

            Console.WriteLine($"Silnia {n} = {fact}");
        }
    }
}