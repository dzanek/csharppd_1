﻿using System;

namespace ConsoleApplication1.Zad3
{
    public static class Zad31
    {
        public static void IsLeapYear()
        {
            Console.WriteLine("Podaj rok:");
            var year = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine(CheckIfLeap(year)
                ? $"Rok: {year} jest rokiem przestępnym."
                : $"Rok: {year} nie jest rokiem przestępnym.");
        }

        static bool CheckIfLeap(int year)
        {
            return year % 4 == 0 && (year % 100 != 0 || year % 400 == 0);
        }
    }
}
