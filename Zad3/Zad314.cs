﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleApplication1.Zad3
{
    public static class Zad314
    {
        public static void PerfectNumbers()
        {
            var perfectNumbers = new List<int>();
            Console.WriteLine("Liczby doskonałe z przedziału <1,n>, podaj n: ");
            var n = Convert.ToInt32(Console.ReadLine());
            for (var i = 1; i <= n; i++)
            {
                var divisors = GetNumberDivisors(i);
                if (divisors.Sum(divisor => divisor < i ? divisor : 0) == i)
                {
                    perfectNumbers.Add(i);
                }
            }
            Console.Write($"Liczby doskonałe z przedziału <1,{n}> : ");
            perfectNumbers.ForEach(pn => Console.Write("{0} ", pn));
        }

        private static IEnumerable<int> GetNumberDivisors(int number)
        {
            var divisors = new List<int>();
            for (var i = 1; i <= Math.Sqrt(number); i++)
            {
                if (number % i != 0) continue;
                divisors.Add(i);
                if (i != number / i)
                {
                    divisors.Add(number / i);
                }
            }
            return divisors;
        }
    }
}