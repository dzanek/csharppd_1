﻿using System;
using ConsoleApplication1.Zad3.exceptions;

namespace ConsoleApplication1.Zad3
{
    public static class Zad34
    {
        public static void SimpleCalculator()
        {
            Console.WriteLine("Podaj pierwszą liczbę:");
            var a = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Podaj drugą liczbę:");
            var b = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Podaj znak operacji:");
            var operation = Convert.ToChar(Console.ReadLine() ?? string.Empty);
            double result;
            try
            {
                result = Calculate(a, b, operation);
            }
            catch (Exception ex) when (
                ex is DivideByZeroException
                || ex is UnsupportedOperationException)
            {
                Console.WriteLine(ex.Message);
                return;
            }
            Console.WriteLine($"{a} {operation} {b} = {result}");
        }

        private static double Calculate(double a, double b, char operation)
        {
            switch (operation)
            {
                case '+':
                    return a + b;
                case '-':
                    return a - b;
                case '*':
                    return a * b;
                case '/':
                    if (b == 0)
                    {
                        throw new DivideByZeroException();
                    }
                    else
                    {
                        return a / b;
                    }
                default:
                    throw new UnsupportedOperationException($"Operacja {operation} nie jest wspierana.");
            }
        }
    }
}