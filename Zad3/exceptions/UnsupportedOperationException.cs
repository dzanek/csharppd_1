﻿using System;

namespace ConsoleApplication1.Zad3.exceptions
{
    public class UnsupportedOperationException : Exception
    {
        public UnsupportedOperationException()
        {
        }

        public UnsupportedOperationException(string message)
            : base(message)
        {
        }

        public UnsupportedOperationException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}