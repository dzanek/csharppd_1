﻿using System;
using System.Linq;

namespace ConsoleApplication1.Zad3
{
    public static class Zad33
    {
        public static void FindAndPrintBiggestNumber()
        {
            Console.WriteLine("Podaj pierwszą liczbę:");
            var a = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Podaj drugą liczbę:");
            var b = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Podaj trzecią liczbę:");
            var c = Convert.ToInt32(Console.ReadLine());
            var biggestNumber = Max(a, b, c);
            Console.WriteLine($"Największa z liczb {a}, {b}, {c} to : {biggestNumber}");
        }
        static T Max<T>(params T[] numbers)
        {
            return numbers.Max();
        }
    }
}
