﻿using System;

namespace ConsoleApplication1.Cw6
{
    public static class Cw61
    {
        private const double A = 14.5;
        private const double B = 24.45;

        public static void PrintVariables()
        {
            Console.Write(A + "\n" + B);
        }
    }
}