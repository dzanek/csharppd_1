﻿using System;

namespace ConsoleApplication1.Cw6
{
    public static class Cw62
    {
        public static void PrintAbc()
        {
            Console.WriteLine(" /---\\  " + "|----\\  " + "/----\\");
            Console.WriteLine("|     | " + "|     | " + "|");
            Console.WriteLine("|     | " + "|     / " + "|");
            Console.WriteLine("|-----| " + "|----   " + "|");
            Console.WriteLine("|     | " + "|     \\ " + "|");
            Console.WriteLine("|     | " + "|     | " + "|");
            Console.WriteLine("|     | " + "|-----/ " + "\\----/");
        }
    }
}