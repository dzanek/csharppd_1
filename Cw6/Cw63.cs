﻿using System;

namespace ConsoleApplication1.Cw6
{
    public static class Cw63
    {
        public static void PrintEquations()
        {
            Console.Write("{0}\t+\t{1}\t=\t{2}\n" +
                          " + \t\t + \t\t + \t\t\n" +
                          "{2}\t+\t{3}\t=\t{4}\n" +
                          " = \t\t = \t\t = \t\t\n" +
                          "{3}\t+\t{5}\t=\t{6}",
                111, 222, 333, 444, 777, 666, 1110);
            
        }
    }
}
