﻿using System;
using ConsoleApplication1.Cw6;
using ConsoleApplication1.Zad1;
using ConsoleApplication1.Zad2;
using ConsoleApplication1.Zad3;

namespace ConsoleApplication1
{
    internal static class Program
    {
        public static void Main()
        {
            //---------------------------------------Cw6
            Console.WriteLine("Cw. 6.1:");
            Cw61.PrintVariables();
            Console.WriteLine("\nCw. 6.2:");
            Cw62.PrintAbc();
            Console.WriteLine("\nCw. 6.3:");
            Cw63.PrintEquations();
            //--------------------------------------Zad1
            Console.WriteLine("\nZad. 1.1:");
            Zad11.SolveAndPrintSolution();
            Console.WriteLine("\nZad. 1.2:");
            Zad12.PerformBitShiftingAndPrintResult();
            Console.WriteLine("\nZad. 1.3:");
            Zad13.PrintBiteOperationsResults();
            Console.WriteLine("\nZad. 1.4:");
            Zad14.PrintXorResult();
            Console.WriteLine("\nZad. 1.5:");
            Zad15.Plus3Operations();
            //---------------------------------------Zad2
            Console.WriteLine("\nZad. 2.1:");
            Zad21.CelsiusToFahrenheit();
            Console.WriteLine("\nZad. 2.2:");
            Zad22.CalculateDelta();
            Console.WriteLine("\nZad. 2.3:");
            Zad23.CalculateBmi();
            Console.WriteLine("\nZad. 2.4:");
            Zad24.PrintResult();
            //---------------------------------------Zad3
            Console.WriteLine("\nZad. 3.1:");
            Zad31.IsLeapYear();
            Console.WriteLine("\nZad. 3.2:");
            Zad32.IsNumberDivisorOfAnother();
            Console.WriteLine("\nZad. 3.3:");
            Zad33.FindAndPrintBiggestNumber();
            Console.WriteLine("\nZad. 3.4:");
            Zad34.SimpleCalculator();
            Console.WriteLine("\nZad. 3.5:");
            Zad35.FindEquationSolutions();
            Console.WriteLine("\nZad. 3.6:");
            Zad23.CalculateBmi(); // 3.6 ma taką samą treść jak zadanie 2.3
            Console.WriteLine("\nZad. 3.10:");
            Zad310.Factorial();
            Console.WriteLine("\nZad. 3.11:");
            Zad311.HowManyNumbers();
            Console.WriteLine("\nZad. 3.12:");
            Zad312.ReadNumbersTillZero();
            Console.WriteLine("\nZad. 3.13:");
            Zad313.SumOfSeries();
            Console.WriteLine("\nZad. 3.14:");
            Zad314.PerfectNumbers();
            Console.WriteLine("\nZad. 3.15:");
            Zad315.Coins();
        }
    }
}
