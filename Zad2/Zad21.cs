﻿using System;

namespace ConsoleApplication1.Zad2
{
    public static class Zad21
    {
        public static void CelsiusToFahrenheit()
        {
            Console.WriteLine("Podaj temperature w stopniach Celsiusza:");
            var celsiusTemp = Convert.ToDouble(Console.ReadLine());
            var fahrenheitTemp = ConvertTemperatureFromCelsiusToFahrenheit(celsiusTemp);
            Console.WriteLine($"{celsiusTemp} C = {fahrenheitTemp} F");
        }

        static double ConvertTemperatureFromCelsiusToFahrenheit(double celsiusTemp)
        {
            return 32 + celsiusTemp * 9 / 5;
        }
    }
}