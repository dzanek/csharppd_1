﻿using System;

namespace ConsoleApplication1.Zad2
{
    public static class Zad22
    {
        public static void CalculateDelta()
        {
            double a, b, c, delta;
            Console.WriteLine("Podaj współczynniki równania kwadratowego:");
            Console.WriteLine("a:");
            a= Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("b:");
            b= Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("c:");
            c= Convert.ToDouble(Console.ReadLine());
            delta = Calculate(a, b, c);
            Console.WriteLine($"Delta = {delta}");
        }

        static double Calculate(double a, double b, double c)
        {
            return b * b - 4 * a * c;
        }
    }
}