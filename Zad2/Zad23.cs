﻿using System;

namespace ConsoleApplication1.Zad2
{
    public static class Zad23
    {
        public static void CalculateBmi()
        {
            Console.WriteLine("Podaj swoją wagę w kilogramach:");
            var weight = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Podaj swój wzrost w metrach:");
            var height = Convert.ToDouble(Console.ReadLine());
            var bmi = Calculate(weight, height);
            Console.WriteLine($"BMI = {bmi}");
        }
        static double Calculate(double weight, double height)
        {
            return weight / (height * height);
        }
    }
}