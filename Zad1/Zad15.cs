﻿using System;

namespace ConsoleApplication1.Zad1
{
    public static class Zad15
    {
        public static void Plus3Operations()
        {
            int a = 1, b = 1, c = 1;
            a <<= 2;
            Console.WriteLine($"#1 : {a}");
            b -= -3;
            Console.WriteLine($"#2 : {b}");
            c *= 4;
            Console.WriteLine($"#3 : {c}");
        }
    }
}