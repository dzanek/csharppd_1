﻿namespace ConsoleApplication1.Zad1
{
    public static class Zad12
    {
        public static void PerformBitShiftingAndPrintResult()
        {
            int a = 256;
            a >>= 2;
            a <<= 2;
            System.Console.WriteLine($"Result: {a}");
        }
    }
}