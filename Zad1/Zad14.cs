﻿namespace ConsoleApplication1.Zad1
{
    public static class Zad14
    {
        public static void PrintXorResult()
        {
            int a = 10;
            a ^= 5;
            System.Console.WriteLine($"XOR result : {a}");
        }
    }
}