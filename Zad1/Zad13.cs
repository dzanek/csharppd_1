﻿namespace ConsoleApplication1.Zad1
{
    public static class Zad13
    {
        public static void PrintBiteOperationsResults()
        {
            int a = 10, b = 16;
            var bitwiseOr = a | b;
            var bitwiseAnd = a & b;
            System.Console.WriteLine($"Suma bitowa: {bitwiseOr}");
            System.Console.WriteLine($"Iloczyn bitowy: {bitwiseAnd}");
        }
    }
}